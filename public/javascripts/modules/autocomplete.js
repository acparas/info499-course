export default function autocomplete(input, latInput, lngInput) {
  if (!input) {
    return;
  }

  const dropdown = new google.maps.places.Autocomplete(input);

  dropdown.addListener('place_changed', () => {
    const place = dropdown.getPlace();
    latInput.value = place.geometry.location.lat();
    lngInput.value = place.geometry.location.lng();
  });

  // People will typically hit "Enter" to pick an address from the dropdown.
  // But that will also trigger the form submission. So let's prevent that.

  input.on('keydown', evt => {
    if (evt.code === 'Enter') {
      evt.preventDefault();
    }
  });
}
