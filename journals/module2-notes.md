# Module 2: Core Concepts - Notes

## Video 1 - Routing
In Module 2's first video, we discuss routing with Express. As a quick TL;DR to
myself, routing is basically how to server responds to HTTP requests to certain
URLS - e.g. a request to `/<some path>`.

To set up routing with Express, first you get the express instance by
```
const express = require('express');
```

Then you get the router property from the express instance
```
const router = express.Router();
```

The reason I can access `req.query.<propName>` is because of **middlewares**
that operate upon the raw request. By the time I can access the data in my route
handlers, that data has been transformed by the middlewares.

He mentions that we will cover middlewares more later.

Route setups look something like
```
router.get(<path:String>, (req, res) => {
  res.json(<stuff>);
});
```

You can send JSON back via `res.json(<value>)`.
This is probably how we'll be handling AJAX requests, huh?

**You can only respond to a request once**. So basically,
```
router.get(<path:String>, (req, res) => {
  // Choose one of these
  res.json(<stuff>);
  res.render(<stuff>);
  res.send(<stuff>);
});
```
If you try to respond multiple times, Express will throw an error.

You can access query params using `req.query`.
For example, if you sent a request to a URL like
`https://<domain>/path?a=true&b=21&c=hey`

You could access those 3 query parameters & their values via `req.query`
```
router.get('/dummy-path', (req, res) => {
  console.log(req.query.a); // true
  console.log(req.query.b); // 21
  console.log(req.query.c); // hey
});
```

Suppose you have a *varible* route of the form
```
/<path1>/cats/<path2>/<path3>
```
You would like to (1) be able to handle this request and (2) access the values
passed in to the `<pathN>` parameters.

Express calls these path segments **Route Parameters**. You can **declare**
these parameters in the route by prefixing them with a colon `:`. So, to rewrite
the example in an Express compliant syntax,
```
/:path1/cats/:path2/:path3
```

Now, (1) your server can service routes of this form and (2) you can access
these parameters. You can access these parameters via `req.params.<paramName>`

For example, this request, `http://<domain>/i-love/cats/so/much`
with this config

```
router.get('/:path1/cats/:path2/:path3', (req, res) => {
  const str = req.params.path1
    + ' dogs '
    + req.params.path2 + ' '
    + req.params.path3;

  res.send(str); // "i-love dogs so much"
});
```

Wes ends the video saying that, while there are many ways to respond to a
request, e.g.
 - `res.send()`
 - `res.json()`
 - `res.render()`
 - `res.sendFile()`

we'll be using `res.json()` and `res.render()` the most.

To be honest, I had a lot of fun here. Doing backend stuff seemed kinda scary to
me, but I think express makes it really too easy haha.

## Video 2 - Templating w/Pug
This video is primarily about using the templating language, Pug. Pug is a
popular templating language within the Node.js community. I personally don't
like it because its syntax is different from vanilla HTML. It's
whitespace-sensitive and uses other syntactic sugar on top. I think that adding
another language simply adds friction, and in this case, it's particularly
unnecessary b/c vanilla HTML isn't that hard to write, and if you think it's
verbose, most editors have autocomplete for HTML anyway, so you don't need to
fully write out each opening & closing tag.

However, I think Pug is worth learning because (1) Wes uses it for this course,
so if I diverge and use something I might prefer like Mustache, it could lead to
compatibility issues, and (2) it's so popular within the Node.js community. I
would eventually encounter Pug in the future, so I may as well become acquainted
with it now.

In `app.js`, we have the line:
```
app.set('views', path.join(__dirname, 'views'));
```

Which maps the string `views` to the directory `./views` in my repo.
(Since `app.js` is in the project root, `__dirname` resolves to the project
root)

According to the express documentation, `app.set(<key>, <value>)`
can be used to map any name to any value.

Later, you can retrieve the value by calling `app.get(<key>)`.

We also have the line
```
app.set('view engine', 'pug');
```

`view engine` is a reserved `<key>` in express, which sets the view engine to
the given `<value>` which can be 1 of several values like `pug`, `mustache`,
etc.

Example Pug code we walked through
```
.wrapper
  - const upcaseDog = dog.toUpperCase();
  p.hello Hello my dog's name is #{upcaseDog.trim()}
  span#yo Yo!!
  img.dog(src="dog.jpg" alt=`Dog ${dog}`)

h2
  | Hello
  em How are you?
```

1. `.wrapper` creates a `<div>` with a class of `wrapper`. (if you don't give an
  element name, it just assumes `<div>`)

2. `- const upcaseDog = doc.toUpperCase()` you can actually create JS variables
     in the template and use it (as seen on the next line)

3. `p.hello Hello my dog's name is #{upcaseDog.trim()}`
  - creating a `<p>` element with a class of `hello`, and text of
    "Hello my dog's name is <result of interpolation>"
  - `#{upcaseDog.trim()}`: to interpolate a value in a textNode (i.e. not for
    an attribute value), use `#{<value>}`. You can execute arbitrary JS
    expressions in the `#{}`

4. `span#yo Yo!!` creates a `<span>` element with an `id` of `yo` and text
   content `Yo!!`

5. "img.dog(src="dog.jpg" alt=`Dog ${dog}`)"
  - creates an `<img>` element with a `class` of `dog`, a `src` of `dog.jpg`
  - the `alt` part is slightly tricky. The main point here is the `${dog}`
    interpolation. Notice that it's different from normal text interpolation.
    Normal text interpolation uses `#{<value>}`. However, you cannot do that for
    attribute values. For attribute values, you must basically use JS to
    interpolate. For the string that's receiving some interpolation, you encase
    it in backticks, just like you would in JS template literals, then you use
    `${<value>}`, just like you would in JS template literals.

6. `h2` creates an `<h2>` element.

7. `| Hello` creates a textNode with `Hello` in the `<h2>`. You need the `| ` if
   you want the text to be considered as a `textNode`, instead of another
   element.

8. `em How are you?` creates an `<em>` element with the textNode `How are you?`

In Pug, you can extend other layouts. So, you can define a base file that does
the `<html>` and `<head>` grunt work, for instance.

Wes has provided the `layout.pug` base file, which we will use as the base
layout for all pages.

In Pug, `block <name>`. the `block` keyword means that the given section
can be replaced by another template.

So, if you have `base.pug`

```
html
  body
  block part1
  block part2
```

You can do the following in another file, say `extension.pug`
```
extends base

block part1
  p hello

block part2
  ul
    li wow
    li amazing
```

So, `extension.pug` will use the content from `base.pug`, but fill-in `part1`
and `part2` with its own provided content

## Video 3 - Template Helpers

To render a template, you can do
```
res.render(<viewFileName> [, <locals>]);
```

So, for instance,
```
res.render('index', { name: 'Anton', age: 21 });
```

And, if you had a view, `index.pug` like so:
```
p I'm #{name} and I'm #{age} years old.
```

it would render a page with the html
```
<p>I'm Anton and I'm 21 years old.</p>
```

So, you can pass values into a template by putting them in an object as the 2nd
parameter to `res.render`. This object contains `locals` -- i.e. values that
become local to the template.

However, sometimes, you **always** want a value to be available to **all**
templates, and it would tedious to pass it in every time.

For example, if you had the template
```
html
  head
    title #{siteName}

p I'm #{nmae} and I'm #{age} years old.
```

and the `title` portion was static for every template -- then it would be very
annoying to have to pass in `siteName` every time like

```
res.render(<someView>, { ... siteName: 'Same old title', ...});
```

So, there is **another way** to expose **locals** to templates, in such a way
that it makes "global exposure" very easy.

Consider

`helpers.js`

```
module.exports = {
  siteName: 'Same old title'
};
```

---

`app.js`

```
app.use((req, res, next) => {
  res.locals.helpers = helpers;
  next();
});

// ....

res.render('test', { name: 'Anton', age: 21 });
```

---

`views/test.pug`

```
html
  head
    title #{helpers.siteName}

p I'm #{nmae} and I'm #{age} years old.
```

Note the `helper.siteName` part. In `app.js`, we added a **middleware** that
would _inject_ a `helpers` value into every template's `locals` values.

That way, we can access it in every template, without having to manually inject
it each time.

This is something we will do for static values such as site name & other things,
as well as some helper libraries like `moment.js`. We will always inject them
into templates' locals for convenience.

## Video 4 - Controllers & the MVC Pattern
MVC = Model, View, Controller

View: basically the pug templates. What we send to the user.
Model: our data, namely the data in the database.
Controller: middle man that coordinates between Model & View.

This style of code
```
router.get('/', (req, res) => {
  // ...
  res.render(<view>, {
    // locals
  });
});
```
gets very, very messy at large scale, so for our actual, prod-quality code,
we'll always abstract out the callback as a **controller**

Consider

`controllers/controller1.js`

```
const controller1 = {
  func1(req, res) {
    res.render('index');
  }
};

module.exports = controller1;
```

`routes/index.js`

```
const controller1 = require('../controllers/controller1');

router.get('/', controller1.func1);
```

`views/index.pug`

```
html hello
```

## Video 5 - Middleware & Error Handling

Middleware is fundamental to Express.js.
Between receiving a **request** and returning a **response**, there may be a lot
of **middle** work necessary, such as

1. If the request is seeking a static asset, just return the static asset
2. Properly format request body & expose the properties on `req` object
3. Properly encode any URLs on the request
4. Run initial validation on any credentials (e.g. ensure email is in the format
   of an email, ensure that the password is at least 10 char long, etc)
5. Take cookies on the request header `Cookie` and expose them on `req.cookies`
6. Save user session to database
7. Authenticate user
8. Expose helper locals globally (discussed above)
9. Actually handle the request.

Duplicating this work for every request handler would be death. Thus, the
concept of **middleware** exists.

There are 2 types of middleware
1. Handler-specific middleware
2. Global middleware (happens for every request)

Handler-specific middleware can be used as such

```
router.get('/', middlewareFunc1, middlewareFunc2, ..., actualHandler)
```

In this case, before `actualHandler` is invoked, all the preceding middleware
would have executed.

Global middleware generally takes the form of
`app.use([path], callback[, callback...])`

If `[path]` is given, then the `callback`s will only act if the requested path's
**base** matches `[path]`. For example, if `[path]` is `/mypath`, and the
requested path is `/mypath/hello`, then that qualifies as a match, and the
corresponding middleware would be called.

If no `[path]` is specified, then the `callback`s will act for every incoming
request.

`app.js` is mainly for setting up middlewares, which is why you see so many
`app.use()` invocations here.

### Error handling
In express, error handling itself is also a middleware.

Consider that global middleware executes sequentially, so, given
```
app.use(middlewareFunc1);
app.use(middlewareFunc2);
app.use(middlewareFunc3);
app.use(middlewareFunc4);
app.use(middlewareFunc5);

app.use(/, router); // actual handler
```

the middlewares will all execute in order, before finally the actual handler.

A common pattern to handle errors then, is
```
app.use(middlewareFunc1);
app.use(middlewareFunc2);
app.use(middlewareFunc3);
app.use(middlewareFunc4);
app.use(middlewareFunc5);

app.use(/, router); // actual handler

// Start of error handling.
app.use(errorHandlers.notFound);

// If the error passes through the `notFound` handler, then try to handle it
// with these either of these handlers.
if (app.get('env') === 'development') {
  app.use(errorHandlers.developmentErrors);
} else {
  app.use(errorHandlers.productionErrors);
}
```

So, if the request gets past the main handler, then, assuming your main handler
handles all typical cases, the request must be faulty somehow, or there must be
an actual error. In that case, the request will move on to the middlewares after
the main handler, which are in fact the error handlers.
