# Module 8: User Accounts & Authentication

## Video 1 - Creating User Accounts
At this point, we start working on authentication and authorization.
This will allow users to comment on other stores, and also restrict them
to only editing store-entries they created/own.

We will start by creating the login view and route.

```
router.get('/login', userController.loginForm);
```

We create a new controller for the user-management, `userController.js`, since
this is logically a different part of our app than before (the stores, which had
a corresponding controller: `storeController.js`)

We also need to create the login view, so we will do that. However, rather than
writing the login **form** view directly in our `login.pug` (the main page for
logging in), we will abstract that view into a mixin called `loginForm.pug`.
That way, we can add login forms to other parts of our website (e.g. inside of a
popup).

Here's the mixin view
```
mixin loginForm()
  form.form(action="/login" method="POST")
    h2 Login
    label(for="email") Email Address
    input(type="email" name="email")
    label(for="password") Password
    input(type="password" name="password")
    input.button(type="submit" value="Log In →")
```

We use a POST request for the form's request, not a GET, because this data
contains sensitive information. This way, the credentials are sent via HTTP
request body, not in the URL as it would be with a GET request.

At this point, we also need to keep track of users, so we'll add a **User**
model to our database.

For our User model, we need a few extra dependencies:

- md5 (Message Digest 5), for hashing the users' passwords, so we don't store
  plain text passwords in the database, which is a big security no-no.
- `validator` - for validating email addresses (yes, we could just write a
  regex)
- `MongoDB Error Handler` - will be explained later
- `Passport Local Mongoose` - will be explained later.

Here's the user schema.

```
const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    validate: [validator.isEmail, 'Invalid Email Address'],
    required: 'Please Supply an email address'
  },
  name: {
    type: String,
    required: 'Please supply a name',
    trim: true
  }
});
```

- email
  - It's a type: string
  - We want the `email` to be `unique`. I.e. an email address cannot be used
    multiple times for an account.
  - lowercased for normalization
  - trimmed for normalization
  - validation delegated to `validator.isEmail`
- name
  - Also type: string
  - trim: true for normalization
  - We won't lowercase it because capitalization is semantic to people's names.

Notice that the `password` field isn't listed here. That will be handled
primarily using `passport.js`

All we have to do is add
```
userSchema.plugin(passportLocalMongoose, { usernameField: 'email' });
```

This adds all the necessary fields to our MongoDB schema for using passwords.
The options object can take many different parameters, but we're just setting 1
option to use users' email addresses as the username field. Some authentication
strategies give users a separate username for the username field, but we'll just
use their email addresses.

The `MongoDB Error Handler` isn't strictly necessary, however, it lets us
transform the cryptic error that comes from `email: { unique }` to a more
human-friendly error.

To use this package, we do
```
userSchema.plugin(mongodbErrorHandler);
```

We also need to add the ability for users to register, so we will add a
`/register` route and a corresponding view

Here's its pug content

```
extends layout

block content
  .inner
    form.form(action="/register" method="POST")
      h2 Register
      label(for="name") Name
      input(type="text" name="name")
      label(for="email") Email
      input(type="email" name="email")
      label(for="password") Password
      input(type="password" name="password")
      label(for="password-confirm") Confirm Password
      input(type="password" name="password-confirm")
      input.button(type="submit" value="Register →")
```

Like `loginForm.pug`, it uses POST for submitting the user's credentials.

We created the `GET` route handler for `/register`
```
router.get('/register', userController.registerForm);
```

Which gets the register page, but we also need to write the logic for handling
`POST` requests to `/register`.

Handling the `POST` request will require a few steps, the first of which is
**validation**. We will hander validation in our controller by creating a
`validateRegister` middleware that will operate before the main handler logic.

For validation

1. we need to sanitize the `name` field to ensure there aren't any `<script>`
   tags or fishy things, so we'll use `req.sanitizeBody('name')`, which
   sanitizes the `name` property on the request body. `sanitizeBody()` is not
   native to the request object, it's added on by the `expressValidator`
   middleware. Here's the line of middleware that's in `app.js` that mounts this
   middleware and adds several validation methods to the `req` object

  ```
  app.use(expressValidator());
  ```

2. check the name actually exists haha
  ```
  req.checkBody('name', 'You must supply a name!').notEmpty();
  ```

3. Ensure that the given email is an email
  ```
  req.checkBody('email', 'That Email is not valid!').isEmail();
  ```

4. Normalizing emails is trickier. So, emails can take many forms, consider:
  ```
  Wesbos@gmail.com
  WESBOS@gmail.com
  wesbos@gmail.com
  wesbos@googlemail.com
  w.e.s.bos@gmail.com
  wesbos+test@gmail.com
  ```

  These are all different strings, but often, you want to normalize several or
  all of these to the same email address `wesbos@gmail.com`. There are several
  options for granularity on how much you wish to normalize.

  We'll be setting is such that (1) dots are NOT removed from the email address
  (this option is true by default). We will let users make different email
  addresses for a service, simply by modifying dot placement in their email
  address. (2) Remove extension (the + and anything after it) will be false. For
  the same reasons as (1). Considering `gmail` and `googlemail` to be separate
  domains. All 3 of these options are `true` by default, so we'll set them to
  `false`.

  The normalization for the email looks like
  ```
  req.sanitizeBody('email').normalizeEmail({
    gmail_remove_dots: false,
    remove_extension: false,
    gmail_remove_subaddress: false
  });
  ```

5. Check the password isn't empty. We cannot rely on the clientside code
   handling this, because the user could just modify the HTML.

6. Check that the 1st input of the password matches the 2nd input (i.e. the
   password confirmation portion);

At the end of this, we can actually get any validation errors by doing
```
const errors = req.validationErrors();
```

If there are indeed errors, we will flash a message saying the error, then we
will return them to the `/register` page.

We will also repopulate the input fields with their inputs, because it would be
frustrating otherwise. We will re-render the `/register` page, but also pass in
`{ body: req.body }` to achieve this.

```
if (errors) {
  next(errors);
}
```

## Video 2 - Saving Registered Users to the Database
At this point, we actually need to add users to our database. We will create a
controller middleware to handle this. Since this middleware is accessing the
database, it will be an `async` function.

Here's the function
```
exports.register = async (req, res, next) => {
  const user = new User({ email: req.body.email, name: req.body.name });
  const register = promisify(User.register, User);
  await register(user, req.body.password);
  next(); // pass to authController.login
};
```

the `User.register` comes from the passport plugin we added in `User.js`
```
userSchema.plugin(passportLocalMongoose, { usernameField: 'email' });
```

However, User.register takes a callback by default. So, we need to convert it to
a promise, we need to promisify it. Hence, we use the `es6-promisify` library to
do that. Since it's also a method on the `User` object, we need to pass the
`User` object, in order to keep the `this` reference intact.

Hence the line
```
const register = promisify(User.register, User);
```

```
register(user, req.body.password)
```

Will add the user to the database, but it will also store it using an md5 hash,
so that we're not storing plaintext passwords.

Finally, we need to log the user in after registering.

We will create a new controller for this point called `AuthController`, though
in my opinion, this could have been grouped in with `UserController`

We make the `login` function on the `UserController`, which looks like this
```
exports.login = passport.authenticate('local', {
  failureRedirect: '/login',
  failureFlash: 'Failed Login!',
  successRedirect: '/',
  successFlash: 'You are now logged in!'
});
```

The `local` part means that we're using a local **strategy**. Passport lets you
use many different type of authentication schemes, which they call strategies.
There's a strategy for loggin in with Facebook, Google, GitHub, etc. `local`
lets us use the **local** strategy, which means that we're doing the auth with
our our username & password database -- not oursourcing it to a 3rd party.

If they fail the login, then we just redirect them to `/login`. If they're
successful, we redirect them to `/`. Both actions have corresponding flash
messages.

## Video 3 - Virtual Fields, Login: Logout middleware & Protecting Routes
Now, we will make **logout** work, make the **login** form work properly. (after
you login, you get a 404 because we haven't handled POST for `/login`), create
an avatar for the **login** icon, and also ensure that a user is logged in upon
navigating to the **add stores** page (authorization).

1. Login method
```
exports.logout = (req, res) => {
  req.logout();
  req.flash('success', 'You are now logged out!');
  res.redirect('/');
};
```

I love `expressValidator` middleware haha.

2. Handle `POST` for `/login`
Super easy, we just reuse `userController.login` from the registration part.
```
router.get('/login', userController.login);
```

Note: In MongoDB, there exist **virtual fields**. Virtual fields act like normal
fields, but they don't actually exist on the document/entry. They are
automatically **derived** from normal fields upon request.

TIL about **gravatars** WOW. No wonder I've signed up for some sides and they
automatically have a picture of me...

We automatically generate a user's avatar by returning.
```
https://gravatar.com/avatar/${hash}s=200
```

`${hash}` is the MD5 hashed email. `s=200` means "size 200" AKA width & height =
200px.

Finally, we add a `isLoggedIn` function to the `authController`, which we will
use as a middleware to block people from accessing the `add store` page unless
they're logged in.

```
exports.isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    next();
    return;
  }
  req.flash('error', 'Oops you must be logged in to do that!');
  res.redirect('/login');
};
```
