# Module 7: Custom Queries and Hooks

## Video 1 - Using Pre-save Hooks to Make Unique Slugs
We currently make store slugs based on the store's name, but we currently don't
handle the case where multiple stores have the same name, and thus would have
the same slug. Here, we have to handle that case by adding a MongoDB pre-save
hook to automatically generate a unique slug.

If multiple stores have the same name (e.g. McDonalds), we will basically suffix
the slug with a count to ensure uniqueness

E.g. the first store named "McDonalds" will have a slug of `mcdonalds`, the
second store's slug will be `mcdonalds-2`, the third store's slug will be
`mcdonalds-3`, so on and so forth.

Here is the new pre-save hook for stores
```
storeSchema.pre('save', async function (next) {
  if (!this.isModified('name')) {
    next();
    return;
  }

  this.slug = slug(this.name);

  // const slugRegEx = /^(${this.slug})((-[0-9]*$)?)$/i;
  // I tried using the literal syntax above, but it didn't work,
  // I don't know why.

  const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
  const storesWithSlug = await this.constructor.find({ slug: slugRegEx });
  if (storesWithSlug.length) {
    this.slug = `${this.slug}-${storesWithSlug.length + 1}`;
  }

  next();
});
```

So, we look for all stores with a slug of form `<store-name>-<number>?`.

## Video 2 - Custom MongoDB Aggregations
At this point, we will begin working on the **Tags** page, where we can group
stores by tags and persue stores only with given tags.

To get stores by tags, we will need to use custom MongoDB **aggregations**.
After watching the video, these aggregations are basically like SQL queries with
multiple clauses.

First, we add two routes as so
```
router.get('/tags', catchErrors(storeController.getStoresByTag));
router.get('/tags/:tag', catchErrors(storeController.getStoresByTag));
```

Then we add the `getStoresByTag()` controller function
```
exports.getStoresByTag = async (req, res) => {
  const tags = await Store.getTagsList();
  const tag = req.params.tag;
  res.render('tag', {
    title: 'Tags',
    tags,
    tag,
  });
};
```

Also, you can add custom methods to your schema by adding it as a property of
the schema's `statics` property. So, `<schema>.statics.<method-name>`.
Also of note, the method declaration must use a normal function, not an arrow
function, because it requires normal behavior of the `this` keyword.

We use this to make the custom aggregator function:

```
storeSchema.statics.getTagsList = function () {
  return this.aggregate([
    { $unwind: '$tags' },
    { $group: { _id: '$tags', count: { $sum: 1 } } },
    { $sort: { count: -1 } }
  ]);
};
```

So, the `aggregate` function takes in an array of operations, these operations
are like clauses in a SQL query.

`$unwind: $tags` gets all stores' tags and, for each tag, maps that tag to an
instance of its store. So, if a restaurant X has 2 tags "Family-friendly" and
"Open Late", the `$unwind: $tags` operation would return an array of 2 entries.
Both of the entires would be X, but the first entry would only have 1 tag value
"Family-friendly", and the 2nd entry would have the other tag value, "Open
Late". There's probably a word to better describe this operation than I am, but
idk.

The remaining 2 operations are more straightforward. (1) groups all stores of
the same tag and counts them, so we can display something like "13 restaurants
Open Late" in our page. (2) sorts the results by descending order (tag with most
matches comes first).

## Video 3 - Multiple Query Promises with Async/Await
At this point, we've created the tags page and summed the # of restaurants that
match each tag. However, when we actually click on the tag, it doesn't fetch the
restaurants that match that tag. That's what we'll be adding in this section.

We will modify our `getStoresByTag()` to get the stores by a specific tag, in
addition to querying for all existing tags. We don't want to do this 1 after the
other, since they're independent actions, so we will use `Promise.all([...])` to
run both promises concurrently. I already knew about this this though, so I
won't speak much more on it.
