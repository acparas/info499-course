# Module 10: Ajax REST API 1
At this point, we'll be working on search functionality.

## Video 1 - Loading Sample Data
Nothing really happened here. Wes wrote a script to populate our DB with sample
data, and he showed us how to run it in this video.

## Video 2 - JSON endpoints and creating MongoDB Indexes
Currently, when we type into the application's search bar, nothing happens.
Ultimately, what we want is

1. You type in a string into the search bar
2. All the stores with that string in the **name** or **description** will
   appear.

To do this, we'll need to add an API end point to return all the stores that
match that string, then, when the user inputs into the search bar, we send an
AJAX request to that API endpoint, then render the returned stores.

We need to learn about MongoDB indexes.

According to the [MongoDB
Documentation](https://docs.mongodb.com/manual/indexes/),

> Indexes support the efficient execution of queries in MongoDB. Without
> indexes, MongoDB must perform a collection scan, i.e. scan every document in a
> collection, to select those documents that match the query statement. If an
> appropriate index exists for a query, MongoDB can use the index to limit the
> number of documents it must inspect.

If you don't have an index, and you search for `coffee` in the name of a store,
MongoDB will literally loop over all your entries and check for `coffee` in the
name. This is fine for small datasets, but if you have like 1 million stores,
this will perform very slowly.

Indexed fields (fields can be indexes) allow MongoDB to pre-emptively store an
organization for the entires according to that field, so it doesn't have to
perform a raw loop-over every time (maybe the indexing allows MongoDB to sort
the stores easily, and thus do a binary search for O(log(n)) performance? Idk)

We shall index store **names** and **descriptions** as text fields (to allow for
searching & case-sensitivity, but I'm not sure what other datatypes you can
index fields as...)

This is what the indexing looks like in Store.js now
```
const storeSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: 'Please enter a store name!'
  },
  slug: String,
  description: {
    type: String,
    trim: true
  },
  tags: [String],
  created: {
    type: Date,
    default: Date.now
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number,
      required: 'You must supply coordinates!'
    }],
    address: {
      type: String,
      required: 'You must supply an address!'
    }
  },
  photo: String,
  author: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: 'You must supply an author!'
  }
});

// Index `name` and `description`
storeSchema.index({
  name: 'text',
  description: 'text'
});
```

Now, here's what the route & handler looks like for the search functionality.

`route`

```
router.get('/api/search', catchErrors(storeController.searchStores));
```

`handler`

```
exports.searchStores = async (req, res) => {
  const stores = await Store.find(
    {
      $text: {
        $search: req.query.q
      }
    },
    {
      score: { $meta: 'textScore' }
    }
  )
  .sort({
    score: { $meta: 'textScore' }
  })
  .limit(5);

  res.json(stores);
};
```

## Video 3 - Creating an Ajax Search Interface
Now, we focus on the UI.

We'll create a function `typeAhead`, that adds an event listener to the
`<input>` for searching, so that, whenever the user types, it hits the
previously constructed endpoint for all stores that match.

There's not much new here, since I'm fairly familiar with front-end JS.

One thing worth noting was the usage of `dompurify` before we assigned a string
to `innerHTML`, just to ensure that we weren't vulnerable to XSS
