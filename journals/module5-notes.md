# Module 5: Geolocation - Notes

I will begin to be a bit more terse in my writings, since it's taking up a lot
of time, and not everything I write down is noteworthy.

## Video 1 - Saving Latitude & Longitude for each store
In this video, we worked on adding some more fields to our Store model, as well
as added some more inputs populate those fields.

We added a "created" time-field, an address field, and a lat (latitiude) and lng
(longitude) field.

Here is the store Schema now:
```
const storeSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: 'Please enter a store name!'
  },
  slug: String,
  description: {
    type: String,
    trim: true
  },
  tags: [String],
  created: {
    type: Date,
    default: Date.now
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number,
      required: 'You must supply coordinates!'
    }],
    address: {
      type: String,
      required: 'You must supply an address!'
    }
  }
});
```

Apparently, MongoDB has some utilities for working with location & coordinates
built-in, so you can query for, say, location in Australia or something like
that, and it just works out of the box. That's quite cool.

I also learned that you can directly use javascript for attribute values by
enclosing the javascript expression in parentheses.

For example,
```
input(type="text" id="lng" name="location[coordinates][0]" value=(store.location && store.location.coordinates[0]) required)
```

So, this lets us populate the **lat** field, if we're editing a store entry, by
checking if it exists for the current store, and, if it does, using that as the
`input`'s value. If that expression fails, it will just set it to undefined, so
the `value` will just be the empty string. So, this also works for adding
stores, not just editing them.

## Video 2 - Geocoding Data with Google Maps
In this video, we'll be working with Google Maps to make the geolocation input
much easier.

- Autocomplete for the address input
- automatically extracting the **lat** & **lng** from the address. Because, what
  person actually knows those values...?

At this point, we start working with the client-side JS. We will make a function
for autocompletion of addresses.

Wow, just by adding the google maps API as a script to your site
```
<script src="https://maps.googleapis.com/maps/api/js?key=<your-key>&libraries=places"></script>
```

Then doing

```
const dropdown = new google.maps.places.Autocomplete(<input-element>);
```

It automatically adds autocompletion.

This is some beautiful code:

```
  const latInput = $('#lat');
  const lngInput = $('#lng');

  const dropdown = new google.maps.places.Autocomplete(input);

  dropdown.addListener('place_changed', () => {
    const place = dropdown.getPlace();
    latInput.value = place.geometry.location.lat();
    lngInput.value = place.geometry.location.lng();
  });
```

There was 1 edge case had to cover:

People will typically hit "Enter" to pick an address from the dropdown.
But that will also trigger the form submission. So, we had to prevent that.

```
input.on('keydown', evt => {
  if (evt.code === 'Enter') {
    evt.preventDefault();
  }
});
```

The `preventDefault()` prevents that action.

## Video 3 - Quick Data Visualization Tip

We modified the `updateStore()` handler to save coordinates with a type of
`Point`, since it wouldn't do it automatically for store **updates**, only for
store **creations**.
