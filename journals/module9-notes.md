# Module 9: Advanced - Email & Permissions

## Video 1 - Sending email with Node.js
In the previous module, we added a password reset flow. Currently, when the
user enters their email into `Forgot your password?` form, we generate & show
them a URL to visit so they can reset their password. Obviously, this is not
correct, and we want to send a password reset URL to the given email. So, that's
what we'll do in this section.

We'll be using `.pug` files for the email. Also, HTML for emails is notoriously
difficult, particularly from a styling perspective. We'll need to inline all of
our styles in the email HTML.

We'll be using `mailtrap.io` for a fake SMTP server for our development
purposes. In a real, production environment, we'd have a mail server, then use
node.js to have the mail server send out the email. Using mailtrap, we won't
actually send any mail out, but we can see all the mail that's attempting to be
sent via node.js. So, we can get a pretty good view on what emails are being
send, and whether they're correct or not.

Here's an example for sending mail
```
const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  }
});

transport.sendMail({
  from: 'Wes Bos <wesbos@gmail.com>',
  to: 'randy@example.com',
  subject: 'Just YO',
  html: 'Hey I <strong>love</strong> you',
  text: 'Hey I **love you**'
});
```

`nodemailer` is the most popular library for sending emails with Node.js
The `Transport` in `createTransport` is an interface between the program and an
email host. We pass in the domain information such as host & port, then we also
pass in the credentials for a user on the email host.

After we have that, we can attempt to send mail using the `transport.sendMail`
method. Note: some email clients don't accept HTML, so we also have the `text`
field, just in case of that.

Here's the code we have now for the passord reset handler

```
exports.forgot = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    req.flash('error', 'Password reset instruction have been mailed to you.');
    return res.redirect('/login');
  }

  user.resetPasswordToken = crypto.randomBytes(20).toString('hex');
  user.resetPasswordExpires = Date.now() + 3600000;
  await user.save();

  const resetURL = `http://${req.headers.host}/account/reset/${user.resetPasswordToken}`;

  // Focus here
  await mail.send({
    user,
    subject: 'Password Reset Link',
    resetURL,
    filename: 'password-reset'
  });

  req.flash('success', `You have been emailed a password reset link.`);
  res.redirect('/login');
};
```

We've also made `mail.send` like so:

```
const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(
    `${__dirname}/../views/email/${filename}.pug`,
    options
  );
};

exports.send = async options => {
  const html = generateHTML(options.filename, options);

  const mailOptions = {
    from: `Wes Bos <noreply@wesbos.com>`,
    to: options.user.email,
    subject: options.subject,
    html,
    text: 'TBD'
  };

  const sendMail = promisify(transport.sendMail, transport);
  return sendMail(mailOptions);
};
```

This is relatively straightforward. One interesting note is the usage of
`__dirname` in the `renderFile` usage. `__dirname` evaluates to the currently
executing script's containing directory. AKA `__dirname` would evaluate to
`handler/mail.js` as expected. We cannot simply use `.` because that depends on
where `node` was invoked. I.e. if I invoked my app via `node
/Documents/app/start.js`, then `.` would evaluate to `/` (root)

Just in case the user cannot view HTML in their email client, we'll convert the
HTML to a reasonable plaintext equivalent like so

add `htmlToText`
```
const htmlToText = require('html-to-text');

exports.send = async options => {
  const html = generateHTML(options.filename, options);
  const text = htmlToText.fromString(html);

  const mailOptions = {
    from: `Wes Bos <noreply@wesbos.com>`,
    to: options.user.email,
    subject: options.subject,
    html,
    text
  };

  const sendMail = promisify(transport.sendMail, transport);
  return sendMail(mailOptions);
};
```

The `htmlToText` module, in practice, does a very good job of extracting text
from HTML elements and presenting it appropriately as a plain text string.

The last part is inlining our CSS for even more accessibility across email
clients.

We do this using a package called `juice` [https://github.com/Automattic/juice]
which takes an HTML file (along with the CSS files injected via `<link>`), then
inspects the selectors & matches the corresponding elements with their
appropriate CSS. It's very clean and simple to use.

We simply require it
```
const juice = require('juice');
```

then change our `generateHTML` method to
```
const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(
    `${__dirname}/../views/email/${filename}.pug`,
    options
  );

  const htmlWithInlinedStyles = juice(html);
  return htmlWithInlinedStyles;
};
```

## Video 2 - Locking down our application with User Permissions
We need to restrict store editing to only the author of the store entry.
However, we can't currently do that because we don't have a way yet (in our DB)
to relate `Stores` to `Users`. In this section, we'll be taking a look at
relationships in MongoDB and how we can add an `author` field to `Stores` to
achieve authorization in our app.

Here's our new `Store` schema.

```
const storeSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: 'Please enter a store name!'
  },
  slug: String,
  description: {
    type: String,
    trim: true
  },
  tags: [String],
  created: {
    type: Date,
    default: Date.now
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number,
      required: 'You must supply coordinates!'
    }],
    address: {
      type: String,
      required: 'You must supply an address!'
    }
  },
  photo: String,
  author: {
    type: mongoose.Scheme.ObjectId, // Every document in MongoDB has an ID
    ref: 'User', // this field represents a relationship
    required: 'You must supply an author!'
  }
});
```

Now, we've restricted editing to only the entry author by doing this

```
const confirmOwner = (store, user) => {
  if (!store.author.equals(user._id)) {
    throw Error('You must own a store in order to edit it!');
  }
};

exports.editStore = async (req, res) => {
  const store = await Store.findOne({ _id: req.params.id });
  confirmOwner(store, req.user); // This has been added
  res.render('editStore', { title: `Edit ${store.name}`, store });
};

```
