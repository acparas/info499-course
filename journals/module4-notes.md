# Module 4: Control Flow - Notes

## Video 1 - Using Async Await
In the previous video, we were working on the post request handler for `/add`.
We left it as

```
exports.createStore = (req, res) => {
  res.json(req.body);
};
```

for demonstration purposes. But obviously, we don't just want to send the
request back to the user (though that is kinda funny). Upon receiving the store
data from the POST request, we need to add it to the database, which is what
we'll be doing in this video.

Database transactions are asynchronous, so we will be using the beautiful
async/await to perform the transactions.

In `controllers/storeController.js`, we need to access mongoose to perform the
database transactions. To do so, we will import the mongoose **singleton** by

```
const mongoose = require('mongoose');
```

again. We also need to work with the `Store` model we created earlier. Do we
need to require that? Well, no, not really. Because the mongoose instance is a
singleton.

In `start.js`, we have this line

```
require('./models/Store');
```

Which runs the code that defines the `Store` model on the mongoose singleton.
After that runs and adds the `Store` model to the mongoose singleton, we can
reference that model anywhere in our code by doing

```
const Store = mongoose.model('Store');
```

Ok, so like 1/2 of this video was introducing async/await, the history of JS and
callbacks & promises. This was stuff I knew already, so I'm not gonna talk about
it here.

Wes introduces a cool trick for error handling with Async/Await.
When it comes to async/await, the go-to method for error handling is wrapping it
in a `try/catch` block. This is fine, but doesn't look as nice as w/o the
`try/catch` block.

The approach that he shows is wrapping each async handler in a function that
1. Runs the function
2. Catches any errors
3. Passes the error to the next middleware (at the point of the
   mainHandler, the next middleware is the error handler)

This function is `catchErrors` in `handlers/errorHandlers.js`, which looks like:

```
exports.catchErrors = (fn) => {
  return function(req, res, next) {
    return fn(req, res, next).catch(next);
  };
};
```

Then, the usage looks like this:

```
router.post('/add', catchErrors(storeController.createStore));
```

## Video 2 - Flash Messages
What is a "Flash Message"? It's best to explain with an example, I think.

1. You're on a page with a form.
2. You submit the form.
3. You're redirected to another page.
4. There's a banner saying "Form successfully submitted!" or something.
5. OR the banner says "Oh no, your submission failed!"

That banner is supposedly called a **flash message**. It's a banner/dialog that
appears on the **next** page, after you complete an action.

If you think about this, this requires state -- this is a stateful interaction.
There are 2 requests at play.

1. The first request submits the form.
2. The second request loads the new page & shows the flash message.

How can we pass this state between requests? When we load a page, how can we
know to show a flash message? A given request could be from a completely
different user or something.

Answer: sessions. We use sessions to maintain a user's state between requests.
Wes says we will cover that later, but that's how it happens.

To achieve flash messages in this app, the workflow is something like so:

1. We use the `connect-flash` middleware.
2. We register a middleware that checks for any `flash` objects on the incoming
   `req` object. If there are some, we inject them into the locals as
   `req.locals.flashes`.
3. In our templates, we check for `locals.flashes`, if they exist,
   render them.
4. In the form POST handler, we do `reQ.flash(<stuff>)`, which will add a flash
   for the next request (for this user... through the magic of sessions... idk,
   we'll discuss this more later...)

## Video 3 - Querying our Database for Stores
We've added functionality to add stores to our database. Now, we will work on
getting those stores from our database, then displaying them in our app.

This will happen in 2 locations:
  - Home Page
  - Stores Page

So, if we write 1 controller function to do this, we handle routes `/` and
`/stores` with that 1 controller function.

I wrote a controller function, `getStores`, which
1. asynchronously queries the list of `Store` documents in my database
2. injects the store's data into the `stores.pug` file, which creates UI for
   each store by passing each store's data into a mixin, `storeCard.pug`

Since `getStores` is an **asynchronous** operation, I wrapped it in
`catchErrors()` when setting it as a route handler, e.g.

```
router.get('/stores', catchErrors(storeController.getStores));
```

I also set `getStores` as the handler for the root `/` path.

## Video 4 - Creating an Editing Flow for Stores
Now, we need the ability to edit existing stores. We will add a `edit` button to
each store card in the store listing.

The edit action shall have a route, which is something like
`/store/<store-identifier>/edit`.

We can uniquely identify each store by accessing its `_id` property, which is
basically a primary key that MongoDB automatically generates for each store
entry.

So, we can make each store's edit route like
```
/stores/<store-identifier>/edit
```

For each `edit` button, we will show a pencil icon to represent the editing
action. The pencil icon shall be an SVG. We shall handle the SVG in a unique
sort of way. We want to utilize the SVG in an `<svg>` element, not a `<img>`
element, so that way we can manipulate the SVG in more ways. (`<img>` doesn't
offer much SVG manipulation). However, we need to get the SVG code in our
document. Rather than firing a network request for the pencil SVG, we will
actually embed the SVG code in the view before sending it off to the user.
We have a helper function `helpers.icon(<filename>)`, which takes the
`<filename>` and gets its corresponding SVG contents.

We can use that to directly inject the SVG code into our views' html by using
the pug operator `!= <function that returns the SVG>`

So, in the `storeCard.pug`, we will do something like
```
a(href=`/stores/${store._id}/edit`)
  != h.icon('pencil')
```

To directly add the pencil SVG.

Now, we need to create the `edit` route handler.
Since the `<store-identifier>` will be unique each time, we need a way to grab
that portion from the URL and work with it.

That is possible using Express's **route params**. E.g. if you do
`/stores/:id/edit` for your route setup, e.g.

```
router.post('/stores/:id/edit', (req, res) => {
  // ...
});
```

On the `req` object, this will exist: `req.params.id`.

So, we can use that to grab the correct store entry and work on it.
Eventually, we will also need to add authentication to this action (e.g. a user
should not be able to edit a store they didn't add/don't have permission for),
but we'll save that for later.

Note: you can do `<Model>.findOneAndUpdate()` to both find a document AND update
it in one motion.
