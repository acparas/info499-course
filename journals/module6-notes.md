# Module 6: File Handing and Image Resizing

## Video 1 - Uploading & Resizing Images using Middleware
At this point, we want to add the ability for users to upload photos for their stores.

To do this, we need to
1. Add an input to allow users to submit photos
2. Use middleware to resize images appropriately.

We will be using 2 middleware packages to handle these tasks.

1. [Multer](https://github.com/expressjs/multer) for handling
   the photo submission.
2. [JIMP (Javascript Image Manipulation Program)](https://github.com/oliver-moran/jimp)
   for handling the photo resizing.

So, when allowing users to submit files in POST requests, you need to encode
the data (all the form's data) as `multipart/form-data`. Using other types of
media encodings won't work. I don't fully understand. However, to achieve this
encoding on the form data, we just add the HTML attribute
`enctype="multipart/form-data"`

We were writing out the options for `multer`, and they looked like this
```
const multerOptions = {
  storage: multer.memoryStorage(),
  fileFilter(req, res, next) {
    const isPhoto = file.mimetype.startsWith('image/');
    if (isPhoto) {
      next(null, true); // Many callbacks in node are err first, data second.
                        // Same is true here for the next func.
    } else {
      next({ message: "That filetype isn't allowed!" }, false);
    }
  }
};
```

I thought it was kind of odd that we were passing `null`, then `true` to `next()`, but
upon reading documentation elsewhere, I realized that this was a common pattern
in node.js. But I just thought I should mention it here.

We currently do server-side filetype validation, but we also added client-side
validation as a preliminary measure by adding this attribute to the photo-file's
`<input>`: `accept="image/gif, image/png, image/jpeg"`

Now, we need to add resizing logic. Also we need to make the filenames for
photos unique. How do we do this? We will use UUIDs for the filenames. (I wonder
if a duplicate UUID has ever occurred yet..)

We use a module called `uuid` (lol) to do this.

```
npm install uuid
const uuid = require('uuid');
```

Here's the resize function we wrote:

```
exports.resize = async (req, res, next) => {
  if (!req.file) {
    next(); // Go to next middleware
    return;
  }

  const extension = req.file.mimetype.split('/')[1];

  // Create file-name
  req.body.photo = `${uuid.v4()}.${extension}`;

  // Resize
  const photo = await jimp.read(req.file.buffer);
  await photo.resize(800, jimp.AUTO); // Preserve aspect ratio with 800 px of width
  await photo.write(`/public/uploads/${req.body.photo}`);
  next();
};
```

The route config for `/add` (POST) looks like this:
```
router.post('/add',
  storeController.upload,
  catchErrors(storeController.resize),
  catchErrors(storeController.createStore),
);
```

The first 2 funcs are middlewares, the last func is the actual handler.

This line caused me a lot of pain:
```
await photo.write(`./public/uploads/${req.body.photo}`);
```

I accidentally omitted the leading `.` for the current directory, and it didn't
work. took me like 20 min to figure it out :P

## Video 2 - Routing & Templating Single Stores
At this point, we begin creating the views for an individual store.

To-do:
- Make route that has a `:slug` route param
- Find the store in the DB with that slug
- grab its information
- inject it into a template
- render it for the user
- ???
- profit

Something really cool is that we're adding a google maps image with a marker to
show where each restaurant is. We do this by using the google maps api -- we
dynamically create a URL based on the given restaurant's lat & lng, then we set
that URL as the src of an `<img>`. The URL (with templates) looks something like
this

```
https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=14&size=800x150&key=${process.env.MAP_KEY}&markers=${lat},${lng}&scale=2
```
