# Module 1: Introduction & Setup Notes

So, I decided to stick with this course's technology almost entirely because wow
there are a lot of moving parts, and I don't think I can deal with swapping
MongoDB with PostgreSQL or adding in Docker right now. I'm pretty nooby so I'll
just take what I see here.

That said, maybe after I build this app, I'll rewrite it as I see fit.

## Starter Files
There are A LOT of starter files here. Hopefully he'll cover all of them/their
functions in this course because my development experience would feel kind of
artificial if I got to start with all this groundwork already.

## Working with MongoDB
I originally wanted to work with a relational database for this project, but oh
well, it wouldn't hurt to learn MongoDB as well.

I just setup a free, hosted MongoDB instance on MongoDB's Atlas service, then
connected to it using MongoDB Compass, a sort of GUI to run queries on the
database. I don't really know much more than that at the moment, just having my
hand held right now.

## Explanation of imports in app.js
At this point, he's explaining all of the imported modules in `app.js`, which
are:

- express
- express-session
- mongoose
- connect-mongo
- path
- cookie-parser
- body-parse
- passport
- es6-promisify
- connect-flash
- express-validator
- routes (local file for routing)
- helpers (helper utils)
- errorHandlers (local file for error handling)

Starting from the top,

### express
I haven't made anything with Express before, but its name is ubiquitous in the
Node.js and even JavaScript ecosystem, so I have undoubtedly heard of it before
and know roughly what it does.

On the website's documentation, it says that Express is a "Fast, unopinionated,
minimalist web framework for Node.js". Wes expands on the "unopinionated" and
"minimalist" part by saying that the web framework does very little out of the
box, so we'll need to pick and include several different node modules to build
this app -- which probably explains why there are so many other imports!

### express-session
He says we'll use `express-session` to manage sessions, which we'll learn about
later on.

### mongoose
A client for interacting with our MongoDB database.

<br><hr>

He stops explaining all of them at this point, then says we'll learn more about
them later on, and in context.

### start.js
We move from discussing `app.js` to `start.js`

He discusses this line
```
require('dotenv').config({ path: 'variables.env' });
```

So, this line pulls environmental variables from `variables.env`. This file is
interesting because it's not supposed to be committed to the repo, since it
contains sensitive information like the database access credentials. So, this
file should live locally on our server, rather than being committed to the repo.

Next, we discuss
```
mongoose.connect(process.env.DATABASE);
```

This is where we connect to our MongoDB instance. `process.env.DATABASE` will
store a URI for connecting to MongoDB. We don't just put the literal URI here
though because IRL, that would likely change depending on if we're in a dev or
production environment.

```
mongoose.Promise = global.Promise;
```

Supposedly you can configure mongoose to use different promise libraries. We're
just configuring it to use the default one here.

```
mongoose.connection.on('error', (err) => {
  console.error(`🙅 🚫 🙅 🚫 🙅 🚫 🙅 🚫 → ${err.message}`);
});
```

This line is here to print errors into the terminal if mongoose fails to connect
upon startup haha.

```
const app = require('./app');
app.set('port', process.env.PORT || 7777);
const server = app.listen(app.get('port'), () => {
  console.log(`Express running → PORT ${server.address().port}`);
});
```

1. Get app file.
2. Set the app's port to the given port by `process.env.PORT`, or just
   default to 7777.
3. Startup the app & print a status message to the console.

### npm start
In package.json, `npm start` runs the following command:
```
concurrently \"npm run watch\" \"npm run assets\" --names \"💻,📦\" --prefix name
```

Which starts the server & the webpack asset builder. The **concurrently** task
runs them concurrently (no way). Server related output is prefixed with 💻, and
Webpack related output is prefixed with 📦. Pretty neat.

That's the end of the intro module.
So now, I will start on module 2: core concepts.
